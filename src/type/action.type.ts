export interface DefaultActionType {
  type: string
  val: object | number | string | boolean | null | Array<object | number | string | boolean>
}
