export interface ActivityListRequestType {
  activityType: number
  limit?: number
  page?: number
  search?: string
}

export interface ActivityTypeResponseType {
  name: string
  id: number
}
