export interface BasicResponse<T = any> {
  data: T
  message: string
  code: number
}
