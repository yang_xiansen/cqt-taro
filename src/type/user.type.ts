export interface UserLoginRequestType {
  avatar?: string
  code: string
  id?: string
  nickname?: string
  openid?: string
  sex?: number
}

export interface UserUpdateRequestType {
  avatar?: string
  nickname?: string
  sex?: number
  profile?: string
}

export interface UserResponseType {
  avatar: string | null
  isAgent: boolean
  isBan: boolean
  isCompany: boolean
  isMember: boolean | null
  nickname: string | null
  score: number | null
  token: string | null
  uid: string | null
}
