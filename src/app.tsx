import { useEffect } from 'react'
import { useDidShow, useDidHide } from '@tarojs/taro'
import { Provider } from 'react-redux'
import { UserService } from './api/user.api'
import { BasicUtils } from './utils/basic.utils'
import * as UserActionType from './store/type/user.action.type'
import store from './store'

// 全局样式
import './app.scss'

function App(props) {
  // 可以使用所有的 React Hooks
  useEffect(() => {
    main()
  }, [])

  // 对应 onShow
  useDidShow(() => {})

  // 对应 onHide
  useDidHide(() => {})
  const main = async (): Promise<void> | never => {
    BasicUtils.startLoading()
    try {
      const { code, data } = await UserService.userGetInfo()
      if (code === 200) {
        store.dispatch({ type: UserActionType.ADD_USER, val: data })
      }
    } catch (e) {
      console.log(e)
    } finally {
      BasicUtils.endLoading()
    }
  }
  return <Provider store={store}>{props.children}</Provider>
}

export default App
