import React, { useState } from 'react'
import { Tabbar, Image } from '@nutui/nutui-react-taro'
import { View } from '@tarojs/components'
import { Add } from '@nutui/icons-react-taro'
import Taro from '@tarojs/taro'
import { useSelector, useDispatch } from 'react-redux'
import * as BasicActionType from '@/store/type/basic.action.type'
import CustomPopupComponent from '../custom-popup'

const CustomTabbarComponent: React.FC = () => {
  const dispatch = useDispatch()
  const selected = useSelector((state: any) => state.basic.tabSelected)
  const [tabBarList] = useState([
    {
      pagePath: 'pages/index/index',
      text: '首页',
      iconPath: 'static/tab-bar/home.png',
      selectedIconPath: 'static/tab-bar/home_selected.png'
    },
    {
      pagePath: 'pages/goods-list/index',
      text: '商品',
      iconPath: 'static/tab-bar/circle.png',
      selectedIconPath: 'static/tab-bar/circle_selected.png'
    },
    {
      iconPath: 'static/tab-bar/circle.png',
      selectedIconPath: 'static/tab-bar/circle_selected.png'
    },
    {
      pagePath: 'pages/activity-list/index',
      text: '活动',
      iconPath: 'static/tab-bar/preferred.png',
      selectedIconPath: 'static/tab-bar/preferred_selected.png'
    },
    {
      pagePath: 'pages/mine/index',
      text: '我的',
      iconPath: 'static/tab-bar/mine.png',
      selectedIconPath: 'static/tab-bar/mine_selected.png'
    }
  ])

  const [visble, setVisible] = useState<boolean>(false)
  const closeVisible = () => {
    setVisible(false)
  }

  const startVisible = () => {
    setVisible(true)
  }

  const changeTab = (val: number) => {
    console.log(val, 11111)
    console.log(visble)
    switch (val) {
      case 2:
        setVisible(true)
        break
      default:
        Taro.switchTab({
          url: `/${tabBarList[val].pagePath!}`
        })
        dispatch({ type: BasicActionType.CHANGE_TAB, val }, 'basic')
        break
    }
  }
  const renderItem = (): Array<React.ReactNode> => {
    return tabBarList.map((item, index) => {
      if (item.text) {
        return <Tabbar.Item key={item.iconPath} title={item.text} icon={<Image src={index === selected ? `/${item.selectedIconPath}` : `/${item.iconPath}`} width="26" height="26" />} />
      } else {
        // eslint-disable-next-line prettier/prettier
        return <Tabbar.Item key={item.iconPath} icon={<View className="w-[60px] h-[60px] absolute bottom-[-5px] leading-[70px] rounded-[30px] bg-[red]" onClick={startVisible}><Add color="white" size={20} /></View>} />
      }
    })
  }

  const navigatorOtherView = (url: string) => {
    Taro.navigateTo({
      url
    })
  }

  return (
    <>
      <Tabbar safeArea fixed value={selected} onSwitch={(val: number) => changeTab(val)}>
        {renderItem()}
      </Tabbar>
      <CustomPopupComponent position="bottom" visible={visble} closeVisible={() => closeVisible()}>
        <View className="h-[20vh] pt-[1rem] flex justify-around items-center">
          <View>活动笔记</View>
          <View onClick={() => navigatorOtherView('/sub-issue/pages/issue-activity/index')}>发起活动</View>
        </View>
      </CustomPopupComponent>
    </>
  )
}

export default CustomTabbarComponent
