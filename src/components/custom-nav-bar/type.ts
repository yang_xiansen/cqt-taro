export interface CustomNavBarPropType {
  needBack?: boolean
  title?: string | React.ReactNode
  custom?: React.ReactNode
}
