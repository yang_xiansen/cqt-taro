import React, { useState } from 'react'
import { ArrowLeft } from '@nutui/icons-react-taro'
import { View } from '@tarojs/components'
import type { CustomNavBarPropType } from './type'

const CustomNavBarComponent: React.FC<CustomNavBarPropType> = (props) => {
  // 自定义导航栏顶部安全距离
  const [navBarPaddingTop] = useState(() => {
    const { statusBarHeight } = wx.getSystemInfoSync()
    const { top } = wx.getMenuButtonBoundingClientRect()
    console.log(top)
    return statusBarHeight
  })
  // 自定义导航栏高度
  const [navBarHeight] = useState(() => {
    const { height, top } = wx.getMenuButtonBoundingClientRect()
    const { statusBarHeight } = wx.getSystemInfoSync()
    const heightMore = (top - statusBarHeight) * 2
    return height + heightMore
  })
  // 自定义导航栏宽度
  const [navBarWidth] = useState(() => {
    const { left, right } = wx.getMenuButtonBoundingClientRect()
    const { screenWidth } = wx.getSystemInfoSync()
    const paddingX = screenWidth - right
    return left - paddingX
  })
  const [paddingX] = useState(() => {
    const { right } = wx.getMenuButtonBoundingClientRect()
    const { screenWidth } = wx.getSystemInfoSync()
    return screenWidth - right
  })
  return (
    <View style={{ paddingTop: `${navBarPaddingTop}px`, paddingLeft: `${paddingX}px` }} className="box-border w-[100vw]">
      <View className="flex justify-between items-center box-border" style={{ height: `${navBarHeight}px`, width: `${navBarWidth}px`, paddingRight: `${paddingX}px` }}>
        {!props.custom && props.needBack && (
          <View>
            <ArrowLeft />
          </View>
        )}
        {!props.custom && typeof props.title === 'string' && <View className="text-center font-bold flex-1">{props.title}</View>}
        {!props.custom && typeof props.title !== 'string' && props.title}
        {props.custom ?? <></>}
      </View>
    </View>
  )
}

export default CustomNavBarComponent
