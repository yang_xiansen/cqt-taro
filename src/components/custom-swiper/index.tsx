import React from 'react'
import { Swiper, Image } from '@nutui/nutui-react-taro'
import type { SwiperComponentPropType, SwiperItemType } from './type'

const CustomSwiperComponent: React.FC<SwiperComponentPropType> = (props) => {
  const siwterItemClick = (item: SwiperItemType) => {
    if (item.id) {
      if (props.swiperItemClick) {
        return props.swiperItemClick(item)
      }
    }
  }
  const renderItem = (list: Array<SwiperItemType>): Array<React.ReactNode> => {
    return list.map((item, index) => {
      return (
        <Swiper.Item key={item.id ?? index}>
          <Image src={item.url} height="100%" width="100%" radius={item.radius} onClick={() => siwterItemClick(item)} />
        </Swiper.Item>
      )
    })
  }
  return (
    <Swiper indicator={props.indicator} loop={!props.loop} autoPlay={props.autoPlay}>
      {renderItem(props.list)}
    </Swiper>
  )
}

export default CustomSwiperComponent
