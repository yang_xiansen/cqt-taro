export interface SwiperComponentPropType {
  list: Array<SwiperItemType>
  loop?: boolean
  indicator?: boolean
  autoPlay?: boolean
  // eslint-disable-next-line no-unused-vars
  swiperItemClick?: (item: SwiperItemType) => void | Promise<void>
}

export interface SwiperItemType {
  url: string
  path?: string
  id?: string | number
  radius?: string | number
}
