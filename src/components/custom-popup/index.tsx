import React from 'react'
import { Popup } from '@nutui/nutui-react-taro'
import type { CustomPopupPropType } from './type'

const CustomPopupComponent: React.FC<CustomPopupPropType> = (props) => {
  return (
    // eslint-disable-next-line prettier/prettier
    <Popup visible={props.visible} closeable={props.closeable} closeIconPosition={props.closeIconPosition ?? 'top-right'} position={props.position} className="px-[1rem] box-border" onClose={() => props.closeVisible()}>
      {props.children}
    </Popup>
  )
}

export default CustomPopupComponent
