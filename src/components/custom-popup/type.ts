export interface CustomPopupPropType {
  visible: boolean
  closeVisible: () => void
  position?: 'top' | 'bottom' | 'left' | 'right' | 'center'
  closeable?: boolean
  closeIconPosition?: 'top-left' | 'top-right' | 'bottom-left' | 'bottom-right'
  title?: React.ReactNode
  left?: React.ReactNode
  description?: React.ReactNode
  destroyOnClose?: boolean
  round?: boolean
  zIndex?: number | string
  children?: React.ReactNode
}
