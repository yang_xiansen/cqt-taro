import { CSSProperties } from 'react'

export interface InfinityRenderPropType {
  isNeedPullRefresh?: boolean // 是否需要下拉刷新
  pullRefreshStyle?: CSSProperties
  hasMore?: boolean
  onRefresh?: () => Promise<void>
  // eslint-disable-next-line no-unused-vars
  init: (limit: number, size: number) => Promise<void>
  loadingText?: () => React.ReactNode
  loadMoreText?: () => React.ReactNode
  children: React.ReactNode
  limit?: number
  size?: number
}
