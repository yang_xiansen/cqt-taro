import React from 'react'
import { Popup } from '@nutui/nutui-react-taro'
import type { MemberPopupPropType } from '../type/member-popup.type'

const MemberPopupComponent: React.FC<MemberPopupPropType> = (props) => {
  return (
    <Popup visible={props.visible} round className="p-[1rem] w-[80vw]" onClose={() => props.closePopup}>
      {props.children}
    </Popup>
  )
}

export default MemberPopupComponent
