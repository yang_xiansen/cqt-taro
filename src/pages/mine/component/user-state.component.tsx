import React from 'react'
import { Avatar, Button as UButton } from '@nutui/nutui-react-taro'
import { Button, View, Text } from '@tarojs/components'
import { useSelector } from 'react-redux'
import { QrCode, Setting } from '@nutui/icons-react-taro'
import type { UserStatePropsType } from '../type/user-state.type'

const UserStateComponent: React.FC<UserStatePropsType> = (props) => {
  const user = useSelector((state: any) => state.user.user)
  const renderAvatar = (): React.ReactNode => {
    if (user) {
      return <Avatar src={user.avatar} size={70} className="mr-[1rem]" />
    } else {
      return (
        <Button onClick={() => props.userQuickLogin()} className="mr-[1rem]">
          <Avatar src={`${process.env.TARO_APP_IMG_OSS_URL}/mine/member-avatar.png`} size={70} />
        </Button>
      )
    }
  }
  const renderNickName = (): React.ReactNode => {
    if (user) {
      return (
        <View className="flex-1 flex justify-between items-center mb-[1rem]">
          <View className="text-bold flex-1 text-[white]" onClick={() => props.nickNameClick()}>
            {user.nickname}
          </View>
          <View className="flex ml-[2rem]">
            <QrCode size={20} className="mr-[1rem]" color="white" onClick={() => props.showQrCode()} />
            <Setting size={20} color="white" onClick={() => props.jumpToOtherView()} />
          </View>
        </View>
      )
    } else {
      return (
        <View className="flex-1 flex justify-between items-center mb-[1rem]">
          <Button className="w-[50px] text-white" onClick={() => props.userQuickLogin()}>
            登录
          </Button>
          <View className="flex ml-[2rem]">
            <QrCode size={20} className="mr-[1rem]" color="white" onClick={() => props.showQrCode()} />
            <Setting size={20} color="white" onClick={() => props.jumpToOtherView()} />
          </View>
        </View>
      )
    }
  }

  return (
    <View className="flex justify-between pb-[1rem]">
      {renderAvatar()}
      <View className="flex-1">
        {renderNickName()}
        <View className="flex justify-between items-center text-white">
          <View>
            信用分<Text>{user?.score ?? 0}</Text>
          </View>
          <UButton type="primary" fill="outline">
            签到中心
          </UButton>
        </View>
      </View>
    </View>
  )
}

export default UserStateComponent
