import { View } from '@tarojs/components'
import React from 'react'
import { Badge, Grid, GridItemProps } from '@nutui/nutui-react-taro'
import { ArrowRight } from '@nutui/icons-react-taro'
import type { UserOrderPropType } from '../type/user-order.type'

const MineOrderComponent: React.FC<UserOrderPropType> = (props) => {
  const main = (): Array<React.ReactNode> => {
    return props.list.map((item) => {
      return (
        <Grid.Item text={item.title} key={item.id}>
          <Badge value={item.value}>
            <View>123</View>
          </Badge>
        </Grid.Item>
      )
    })
  }
  const girdItemClick = (index: number) => {
    return props.jumpToOrderList(`/sub-goods/pages/user-order/index?type=${index ?? 0}`)
  }
  return (
    <View className="rounded-xl bg-white p-[.5rem]">
      <View className="flex justify-between items-center mb-[1rem] text-[1.1rem]" onClick={() => props.jumpToOrderList('/sub-goods/pages/user-order/index')}>
        <View>我的订单</View>
        <View className="text-[.9rem] text-[#999999]">
          全部
          <ArrowRight size={14} color="#999999" />
        </View>
      </View>
      <Grid style={{ '--nutui-grid-border-color': 'rgba(0,0,0,0)' }} columns={4} onClick={(item: GridItemProps, index: number) => girdItemClick(index)}>
        {main()}
      </Grid>
    </View>
  )
}

export default MineOrderComponent
