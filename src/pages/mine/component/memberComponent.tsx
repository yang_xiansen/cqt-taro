import React from 'react'
import { View } from '@tarojs/components'
import { useSelector } from 'react-redux'

const MemberComponent: React.FC = () => {
  const user = useSelector((state: any) => state.user.user)
  const init = (): React.ReactNode => {
    if (user) {
      return <></>
    } else {
      return <>123</>
    }
  }
  return <View className="bg-[#f4f5f3] p-[1rem]">{init()}</View>
}

export default MemberComponent
