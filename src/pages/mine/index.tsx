import React, { useState } from 'react'
import { View } from '@tarojs/components'
import { Button } from '@nutui/nutui-react-taro'
import Taro from '@tarojs/taro'
import { BasicUtils } from '@/utils/basic.utils'
import { UserService } from '@/api/user.api'
import type { UserLoginRequestType } from '@/type/user.type'
import { useDispatch, useSelector } from 'react-redux'
import * as UserActionType from '@/store/type/user.action.type'
import CustomNavBarComponent from '../../components/custom-nav-bar'
import UserStateComponent from './component/user-state.component'
import MineOrderComponent from './component/mine-order.component'
import MemberPopupComponent from './component/member-popup.component'
import CustomTabbarComponent from '../../components/custom-tab-bar'

const MineView: React.FC = () => {
  const dispatch = useDispatch()
  const user = useSelector((state: any) => state.user.user)
  /** 用户快速登录 */
  const userQuickLogin = async () => {
    try {
      BasicUtils.startLoading()
      const taroLogin = await Taro.login({ timeout: 3000 })
      if (taroLogin.errMsg === 'login:ok') {
        const {
          errMsg,
          userInfo: { avatarUrl, nickName, gender }
        } = await Taro.getUserInfo()
        if (errMsg === 'getUserInfo:ok') {
          const obj: UserLoginRequestType = {
            code: taroLogin.code,
            avatar: avatarUrl,
            nickname: nickName,
            sex: gender
          }
          const { data, message, code } = await UserService.userLogin(obj)
          if (code === 200) {
            Taro.setStorageSync('token', data.token)
            dispatch({ type: UserActionType.ADD_USER, val: data }, 'user')
          } else {
            BasicUtils.showToast(message)
          }
        }
      }
    } finally {
      BasicUtils.endLoading()
    }
  }
  /** 扫码图标点击 */
  const showQrCode = () => {
    console.log(1)
  }
  /** 设置图标点击 */
  const jumpToOtherView = () => {
    jumpToOrderList('/sub-mine/pages/settings/index')
  }
  /** 跳转去其他页面 */
  const jumpToOrderList = (url: string) => {
    Taro.navigateTo({ url })
  }
  /** 订单详情数据列表 */
  const [orderList] = useState([
    {
      id: 1,
      title: '待付款'
    },
    {
      id: 3,
      title: '待收货',
      value: 0
    },
    {
      id: 4,
      title: '待评价',
      value: 0
    },
    {
      id: 5,
      title: '退款/售后',
      value: 0
    }
  ])
  /** 会员展示框开关 */
  const [popupVisible, setPopupVisible] = useState<boolean>(false)
  /** 会员展示框关闭方法 */
  const closeMemberPopup = () => {
    setPopupVisible(false)
  }
  /** 用户名点击 */
  const nickNameClick = () => {
    if (user) {
      setPopupVisible(true)
    } else {
      userQuickLogin()
    }
  }

  return (
    <>
      {/* <View className="bg-gradient-to-tl from-bgForMineTitleStart from-10% to-bgForMineTitleEnd"> */}
      <View className="bg-[#666666]">
        <CustomNavBarComponent />
        <View className="px-[1rem] pt-[.5rem]">
          <UserStateComponent userQuickLogin={userQuickLogin} showQrCode={showQrCode} jumpToOtherView={jumpToOtherView} nickNameClick={nickNameClick} />
        </View>
      </View>
      <View className="bg-[#999999] rounded-tl-xl rounded-tr-xl px-[.5rem] py-[2rem]">
        <MineOrderComponent list={orderList} jumpToOrderList={jumpToOrderList} />
      </View>

      <MemberPopupComponent visible={popupVisible} closePopup={closeMemberPopup}>
        <View>123</View>
        <Button block type="primary">
          充值
        </Button>
      </MemberPopupComponent>
      <CustomTabbarComponent />
    </>
  )
}

export default MineView
