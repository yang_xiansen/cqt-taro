export interface UserStatePropsType {
  userQuickLogin: () => Promise<void>
  showQrCode: () => Promise<void> | void
  jumpToOtherView: () => Promise<void> | void
  nickNameClick: () => void
}
