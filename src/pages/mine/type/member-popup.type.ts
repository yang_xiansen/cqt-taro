export interface MemberPopupPropType {
  visible: boolean
  closePopup: () => void
  children: React.ReactNode
}
