import React from 'react'
import CustomTabbarComponent from '../../components/custom-tab-bar'
import { GoodsService } from '../../api/goods.api'

const GoodsListView: React.FC = () => {
  const main = async (page: number, size: number) => {
    console.log(page, size)
    const data = await GoodsService.getGoodsTypeList()
    console.log(data)
  }
  main(1, 10)
  return (
    <>
      商品列表
      <CustomTabbarComponent />
    </>
  )
}

export default GoodsListView
