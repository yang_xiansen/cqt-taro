import React, { useState, useEffect } from 'react'
import { View } from '@tarojs/components'
import { Tabs } from '@nutui/nutui-react-taro'
import { ActivityService } from '@/api/activity.api'
import { BasicUtils } from '@/utils/basic.utils'
import type { ActivityListRequestType, ActivityTypeResponseType } from '@/type/activity.type'
import type { SwiperItemType } from '@/components/custom-swiper/type'
import CustomTabbarComponent from '../../components/custom-tab-bar'
import CustomSwiperComponent from '../../components/custom-swiper'
import CustomInfinityRenderComponent from '../../components/custom-infinity-render'

const ActivityListView: React.FC = () => {
  const [activityType, setActivityType] = useState<Array<ActivityTypeResponseType>>([])
  const getActivityTypeList = async () => {
    try {
      const { data, code, message } = await ActivityService.getActivityType()
      if (code === 200) {
        setActivityType(data)
        return Promise.resolve(data[0]?.id)
      } else {
        BasicUtils.showToast(message)
        return Promise.resolve(0)
      }
    } catch (e) {
      return Promise.reject(0)
    }
  }
  useEffect(() => {
    init()
  }, [])
  const init = async () => {
    const data = await getActivityTypeList()
    if (data) {
      await dataInit(1, 10, data)
    }
  }
  // getActivityTypeList()
  const [swiperList] = useState<Array<SwiperItemType>>([
    {
      url: 'https://storage.360buyimg.com/jdc-article/welcomenutui.jpg',
      radius: 8
    }
  ])
  const mainInit = async (limit: number, size: number) => {
    dataInit(limit, size, activityType[tabActive].id)
  }
  const dataInit = async (limit: number, size: number, type: number) => {
    try {
      BasicUtils.startLoading()
      const obj: ActivityListRequestType = {
        limit,
        page: size,
        activityType: type
      }
      const { data, code } = await ActivityService.getActivityList(obj)
      if (code === 200) {
        console.log(data)
      }
    } finally {
      BasicUtils.endLoading()
    }
  }
  const [tabActive, setTabActive] = useState<number>(0)
  const changeTab = async (value: number) => {
    setTabActive(value)
    await dataInit(1, 10, activityType[tabActive].id)
  }

  return (
    <>
      <View className="px-[1rem]">
        <CustomSwiperComponent list={swiperList} />
        <Tabs value={tabActive} onChange={(value: number) => changeTab(value)}>
          {activityType.map((item) => {
            return <Tabs.TabPane title={item.name} key={item.id} />
          })}
        </Tabs>
        <CustomInfinityRenderComponent init={(limit: number, size: number) => mainInit(limit, size)} isNeedPullRefresh>
          <View className="bg-[red]">123123123</View>
        </CustomInfinityRenderComponent>
      </View>
      <CustomTabbarComponent />
    </>
  )
}

export default ActivityListView
