import { View } from '@tarojs/components'
import { Button } from '@nutui/nutui-react-taro'
import { useSelector } from 'react-redux'
import './index.scss'
import CustomTabbarComponent from '../../components/custom-tab-bar'

function Index() {
  const user = useSelector((state: any) => state.user.user)
  console.log(user, 1111)
  return (
    <>
      <View className="nutui-react-demo">
        <View className="index">欢迎使用 NutUI React 开发 Taro 多端项目。</View>
        <View className="index">
          <Button type="primary" className="btn">
            NutUI React Button
          </Button>
        </View>
        <View className="bg-[red]">123</View>
      </View>
      <CustomTabbarComponent />
    </>
  )
}

export default Index
