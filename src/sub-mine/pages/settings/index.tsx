import React from 'react'
import Taro from '@tarojs/taro'
import { View } from '@tarojs/components'
import { Button } from '@nutui/nutui-react-taro'
import { ArrowRight } from '@nutui/icons-react-taro'
import { useSelector, useDispatch } from 'react-redux'
import * as UserActionType from '@/store/type/user.action.type'
import { UserService } from '@/api/user.api'
import { BasicUtils } from '@/utils/basic.utils'
import { DEFAULT_ICON_COLOR } from '@/config/default.config'
// import Style from '@/static/css/global.scss'

const UserSettingView: React.FC = () => {
  const user = useSelector((state: any) => state.user.user)
  const dispatch = useDispatch()
  /** 退出登录 */
  const logOut = async () => {
    console.log(Style)
    try {
      BasicUtils.startLoading()
      if (user) {
        const { code, message } = await UserService.userLogOut()
        if (code === 200) {
          dispatch({ type: UserActionType.ADD_USER, val: null })
          Taro.removeStorageSync('token')
        } else {
          BasicUtils.showToast(message)
        }
      } else {
        BasicUtils.showToast('您尚未登录')
      }
    } finally {
      BasicUtils.endLoading()
    }
  }

  const navigateOtherView = (url: string) => {
    Taro.navigateTo({
      url
    })
  }

  return (
    <View className="px-[1rem]">
      <View className="rounded-[.5rem] bg-white mb-[1rem] p-[1rem] mt-[1rem]">
        <View className="flex justify-between items-center py-[.5rem] border-b-[#f4f5f3] border-b-[1px]" style={{ borderBottomStyle: 'solid' }}>
          <View className="flex-1">服务条款</View>
          <View className="w-[20px] h-[20px]">
            <ArrowRight size={20} color={DEFAULT_ICON_COLOR} />
          </View>
        </View>
        <View className="flex justify-between items-center py-[.5rem]" onClick={() => navigateOtherView('/sub-mine/pages/user-update/index')}>
          <View className="flex-1">用户设置</View>
          <View className="w-[20px] h-[20px]">
            <ArrowRight size={20} color={DEFAULT_ICON_COLOR} />
          </View>
        </View>
      </View>
      <View className="fixed bottom-[40px] pb-safe w-[calc(100%-2rem)]">
        <Button block type="primary" onClick={logOut}>
          退出登录
        </Button>
      </View>
    </View>
  )
}

export default UserSettingView
