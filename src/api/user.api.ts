/**
 * @TODO 用户分类接口调用
 */
import type { UserResponseType, UserLoginRequestType, UserUpdateRequestType } from '../type/user.type'
import type { BasicResponse } from '../type/basic.type'
import request from '../utils/request.utils'

const BASE_URL = {
  WX_LOGIN: 'front/wxLogin', // 微信登录
  GET_USER: 'front/mine/getUserInfo', // 获取用户详情
  UPDATE_USER: 'front/mine/updateUserInfo', // 修改用户详情
  GET_MINE_FANS: 'front/mine/getFollowMineUserInfos', // 关注我的人
  GET_MINE_FOLLOW: 'front/mine/getUserFollowUserInfos', // 我关注的人
  LOG_OUT: 'front/logout' // 退出登录
}

export class UserService {
  /** 微信登录 */
  static async userLogin(params: UserLoginRequestType): Promise<BasicResponse<UserResponseType>> {
    return request.post(BASE_URL.WX_LOGIN, params)
  }

  /** 修改用户信息 */
  static async updateUserInfo(params: UserUpdateRequestType): Promise<BasicResponse> {
    return request.post(BASE_URL.UPDATE_USER, params)
  }

  /** 获取用户详情 */
  static async userGetInfo(): Promise<BasicResponse<UserResponseType>> {
    return request.post(BASE_URL.GET_USER)
  }

  /** 获取我的粉丝 */
  static async userGetFans(): Promise<BasicResponse> {
    return request.post(BASE_URL.GET_MINE_FANS)
  }

  /** 获取我关注的人 */
  static async userGetFollow(): Promise<BasicResponse> {
    return request.post(BASE_URL.GET_MINE_FOLLOW)
  }

  /** 退出登录 */
  static async userLogOut(): Promise<BasicResponse> {
    return request.get(BASE_URL.LOG_OUT)
  }
}
