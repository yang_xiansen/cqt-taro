import Taro from '@tarojs/taro'

import { BasicUtils } from '../utils/basic.utils'
import request from '../utils/request.utils'
import type { BasicResponse } from '../type/basic.type'

const BASE_URL = {
  UPLOAD_FILE: 'front/images/uploadjdoss', // 文件上传
  DELETE_IMAGE: 'front/images/deleFile/'
}

export class BasicService {
  /** 文件上传 */
  static async fileUpload(filePath: string, name: string): Promise<BasicResponse> {
    const token = BasicUtils.getStorage('token')
    const header = {}
    if (token) {
      header['Authori-Zation'] = token
    }
    return new Promise((resolve, reject) => {
      debugger
      Taro.uploadFile({
        url: `${process.env.TARO_APP_BASE_URL}api/${BASE_URL.UPLOAD_FILE}`,
        header,
        filePath,
        name,
        success: (res) => {
          resolve(JSON.parse(res.data) as unknown as BasicResponse)
        },
        fail: (err) => {
          reject(err)
        }
      })
    })
  }

  /** 文件删除 */
  static async deleteFile(fileName: string): Promise<BasicResponse> {
    return request.get(`${BASE_URL.DELETE_IMAGE}${fileName}`)
  }
}
