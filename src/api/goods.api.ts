import type { BasicResponse } from '../type/basic.type'
import request from '../utils/request.utils'

const BASE_URL = {
  GOODS_LIST: 'front/goods/getGoodsList', // 获取商品列表
  GOODS_TYPE: 'front/goods/getGoodsType' // 获取商品分类
}

export class GoodsService {
  /** 获取商品分类 */
  static async getGoodsTypeList() {
    return request.post(BASE_URL.GOODS_TYPE)
  }

  /** 获取商品列表 */
  static async getGoodsList(params: any): Promise<BasicResponse> {
    return request.get(BASE_URL.GOODS_LIST, params)
  }
}
