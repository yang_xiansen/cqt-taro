import type { BasicResponse } from '../type/basic.type'
import type { ActivityTypeResponseType, ActivityListRequestType } from '../type/activity.type'
import request from '../utils/request.utils'

const BASE_URL = {
  ACTIVITY_LIST: 'front/activity/getActivityList', // 获取活动列表
  ACTIVITY_TYPE: 'front/activity/getActivityType' // 获取活动类型
}

export class ActivityService {
  /** 获取活动类型 */
  static async getActivityType(): Promise<BasicResponse<Array<ActivityTypeResponseType>>> {
    return request.post(BASE_URL.ACTIVITY_TYPE)
  }
  /** 获取活动列表 */
  static async getActivityList(params: ActivityListRequestType): Promise<BasicResponse> {
    return request.post(BASE_URL.ACTIVITY_LIST, params, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
  }
}
