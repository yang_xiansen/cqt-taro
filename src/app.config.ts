export default defineAppConfig({
  pages: ['pages/index/index', 'pages/mine/index', 'pages/goods-list/index', 'pages/activity-list/index'],
  subPackages: [
    {
      root: 'sub-mine',
      pages: ['pages/user-update/index', 'pages/settings/index']
    },
    {
      root: 'sub-goods',
      pages: ['pages/user-order/index']
    },
    {
      root: 'sub-issue',
      pages: ['pages/issue-activity/index']
    }
  ],
  tabBar: {
    custom: true,
    selectedColor: '#248067',
    color: '#333',
    borderStyle: 'white',
    list: [
      {
        pagePath: 'pages/index/index',
        text: '首页',
        iconPath: 'static/tab-bar/home.png',
        selectedIconPath: 'static/tab-bar/home_selected.png'
      },
      {
        pagePath: 'pages/goods-list/index',
        text: '商品',
        iconPath: 'static/tab-bar/circle.png',
        selectedIconPath: 'static/tab-bar/circle_selected.png'
      },
      {
        pagePath: 'pages/activity-list/index',
        text: '活动',
        iconPath: 'static/tab-bar/preferred.png',
        selectedIconPath: 'static/tab-bar/preferred_selected.png'
      },
      {
        pagePath: 'pages/mine/index',
        text: '我的',
        iconPath: 'static/tab-bar/mine.png',
        selectedIconPath: 'static/tab-bar/mine_selected.png'
      }
    ]
  },
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  }
})
