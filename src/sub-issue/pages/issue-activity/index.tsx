import React, { useState, useEffect, useRef } from 'react'
import { View } from '@tarojs/components'
import { Uploader, Form, Button, Input, TextArea, Picker, Cell, Row, Col, InputNumber } from '@nutui/nutui-react-taro'
import { ActivityService } from '@/api/activity.api'
import { BasicUtils } from '@/utils/basic.utils'
import { ArrowRight } from '@nutui/icons-react-taro'

const InssueActivityView: React.FC = () => {
  const [uploadUrl] = useState<string>(() => {
    return `${process.env.TARO_APP_BASE_URL}api/front/images/uploadjdoss`
  })
  const formRef = useRef(null)
  const submitFooter = (): React.ReactNode => {
    return (
      <Button formType="submit" block type="primary">
        提交
      </Button>
    )
  }
  const [pickerOptions, setPickerOptions] = useState<Array<{ value: number; text: string }>>([])
  const init = async () => {
    const { data, code, message } = await ActivityService.getActivityType()
    if (code === 200) {
      const asyncData = JSON.parse(JSON.stringify(data))
      const list: Array<{ value: number; text: string }> = []
      asyncData.forEach((item) => {
        list.push({ value: item.id, text: item.name })
      })
      setPickerOptions(list)
    } else {
      BasicUtils.showToast(message)
    }
  }
  const getValueFromEvent = (...args) => {
    console.log(args)
    return args[1]
  }
  const pickerClick = (event: any, ref: any) => {
    console.log(event)
    ref.open()
  }
  useEffect(() => {
    init()
  }, [])
  const renderPicker = (value: any) => {
    return <Cell className="nutui-cell--clickable" title={value.length ? pickerOptions!.filter((po) => po.value === value[0])[0]?.text : 'Please select'} extra={<ArrowRight />} align="center" />
  }
  const submitForm = (val: any) => {
    console.log(val)
  }
  return (
    <>
      <View>
        <Uploader url={uploadUrl} multiple maxCount="5" />
      </View>
      <Form ref={formRef} footer={submitFooter()} onFinish={submitForm}>
        <Form.Item name="title">
          <Input />
        </Form.Item>
        <Form.Item name="content">
          <TextArea />
        </Form.Item>
        <Form.Item name="type" trigger="onConfirm" getValueFromEvent={(...args) => getValueFromEvent(args)} onClick={(event, ref: any) => pickerClick(event, ref)}>
          <Picker options={pickerOptions}>{(value: any) => renderPicker(value)}</Picker>
        </Form.Item>
        <Row>
          <Col span={12}>
            <Form.Item label="男" name="man">
              <InputNumber defaultValue={0} />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="女" name="girl">
              <InputNumber defaultValue={0} />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </>
  )
}

export default InssueActivityView
