import Taro from '@tarojs/taro'

export class BasicUtils {
  /** 获取本地存储 */
  static getStorage(key: string): string | Object | ArrayBuffer | null | never {
    return Taro.getStorageSync(key)
  }

  /** 展示吐司 */
  static showToast(message: string, icon: 'none' | 'success' | 'error' | 'loading' = 'none'): void {
    Taro.showToast({
      title: message,
      icon,
      duration: 2500
    })
  }

  /** 全局loading */
  static startLoading(): void {
    Taro.showLoading({
      title: '请稍后'
    })
  }

  /** 关闭loading */
  static endLoading(): void {
    Taro.hideLoading()
  }
}
