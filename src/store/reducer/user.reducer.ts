import type { DefaultActionType } from '../../type/action.type'
import * as actionType from '../type/user.action.type'

interface UserStateType {
  user: any
  fansList: Array<any> | null
  fllowList: Array<any> | null
}

const defaultState: UserStateType = {
  user: null,
  fansList: null,
  fllowList: null
}
const userReducer = (state: UserStateType = defaultState, action: DefaultActionType) => {
  const newState = JSON.parse(JSON.stringify(state))
  switch (action.type) {
    case actionType.ADD_USER:
      newState.user = action.val
      break
    case actionType.ADD_FANS:
      newState.fansList = action.val
      break
    case actionType.ADD_FLLOW:
      newState.fllowList = action.val
      break
    default:
      break
  }
  return newState
}

export default userReducer
