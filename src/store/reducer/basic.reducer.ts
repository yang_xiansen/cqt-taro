import type { DefaultActionType } from '../../type/action.type'
import * as BasicActionType from '../type/basic.action.type'

interface BasicStateType {
  tabSelected: number
}
const defaultState: BasicStateType = {
  tabSelected: 0
}
const basicReducer = (state: BasicStateType = defaultState, action: DefaultActionType) => {
  const newState = JSON.parse(JSON.stringify(state))
  switch (action.type) {
    case BasicActionType.CHANGE_TAB:
      newState.tabSelected = action.val
      break
    default:
      break
  }
  return newState
}

export default basicReducer
