import { legacy_createStore, combineReducers } from 'redux'
import userReducer from './reducer/user.reducer'
import basicReducer from './reducer/basic.reducer'

const rootReducer = combineReducers({
  user: userReducer,
  basic: basicReducer
})
const store = legacy_createStore(rootReducer)

export default store
