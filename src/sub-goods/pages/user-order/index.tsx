import { Tabs } from '@nutui/nutui-react-taro'
import { getCurrentInstance } from '@tarojs/runtime'
import React, { useState } from 'react'

const UserOrderView: React.FC = () => {
  const { router } = getCurrentInstance()
  console.log(router?.params)
  const [orderList] = useState([
    {
      id: 1,
      title: '待付款'
    },
    {
      id: 2,
      title: '待收货'
    },
    {
      id: 3,
      title: '待评价'
    },
    {
      id: 4,
      title: '退款/售后'
    }
  ])
  const renderTabPane = (): Array<React.ReactNode> => {
    return orderList.map((item) => {
      return (
        <Tabs.TabPane title={item.title} key={item.id}>
          {item.title}
        </Tabs.TabPane>
      )
    })
  }
  return <Tabs>{renderTabPane()}</Tabs>
}

export default UserOrderView
