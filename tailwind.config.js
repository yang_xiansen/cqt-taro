/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./public/index.html', './src/**/*.{html,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        bgForMineTitleStart: '#464854',
        bgForMineTitleEnd: '#7e765f',
        bgForMineTitle: 'linear-gradient(45deg, #464854 10%, #7e765f)'
      },
      spacing: {
        safe: 'calc(env(safe-area-inset-bottom)+12px)'
      }
    }
  },
  plugins: []
}
